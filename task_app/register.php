<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
</head>

<body>
    <div class="vh-100 d-flex align-items-center justify-content-center">
        <form class="w-25" id="registerForm">
            <h1 class="text-center">Register</h1>
            <div class="mb-3">
                <label for="inputEmail" class="form-label">Email address</label>
                <input type="email" class="form-control" id="inputEmail" name="inputEmail">

            </div>
            <div class="mb-3">
                <label for="inputPassword" class="form-label">Password</label>
                <input type="password" class="form-control" id="inputPassword" name="inputPassword">
            </div>
            <div class=" my-4">
                <select class="form-select" aria-label="select role" id=role name="role">
                    <option selected>Select role</option>
                    <option value="developer">Developer</option>
                    <option value="project manager">Project Manager</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary w-100">Register</button>
            <div class="my-4 ">
                <a href="./">Login</a>
            </div>
        </form>
    </div>



    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>
</body>

</html>