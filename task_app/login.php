<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
</head>

<body>
    <div class="vh-100 d-flex align-items-center justify-content-center">
        <form class="w-25" id="loginForm">
            <h1 class="text-center">Login</h1>
            <div class="mb-3">
                <label for="inputEmail" class="form-label">Email address</label>
                <input type="email" class="form-control" id="inputEmail" name="inputEmail">

            </div>
            <div class="mb-3">
                <label for="inputPassword" class="form-label">Password</label>
                <input type="password" class="form-control" id="inputPassword" name="inputPassword">
            </div>

            <button type="submit" class="btn btn-primary w-100 font-bold">Login</button>
            <div class="my-4 ">
                <a href="./register.php">Create an account</a>
            </div>
        </form>

    </div>


    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>



</body>

</html>