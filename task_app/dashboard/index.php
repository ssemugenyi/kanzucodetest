<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
</head>

<body>
    <header class="d-flex align-items-center justify-content-between p-3 bg-dark text-white">
        <h1>Task Management App</h1>
        <a href="../" class="text-white">Logout</a>
    </header>

    <div class="d-flex align-items-center justify-content-end w-75 my-3">
        <button class="btn btn-primary" type="button" class="btn btn-primary" data-bs-toggle="modal"
            data-bs-target="#createProject">Create Project</button>
    </div>

    <div>

        <!-- Project list -->

        <table class="table w-75 mx-auto">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Project Name</th>
                    <th scope="col">Engineer</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody id="tableBody">
                <tr>
                    <th scope="row">1</th>
                    <td>Project 1</td>
                    <td>Engineer 1</td>
                    <td>Completed</td>
                    <td>
                        <button class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#editProject">Edit</button>
                        <button class="btn btn-danger">Delete</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>Project 2</td>
                    <td>Engineer 2</td>
                    <td>Completed</td>
                    <td>
                        <button class="btn btn-primary">Edit</button>
                        <button class="btn btn-danger">Delete</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>Project 3</td>
                    <td>Engineer 3</td>
                    <td>Completed</td>
                    <td>
                        <button class="btn btn-primary">Edit</button>
                        <button class="btn btn-danger">Delete</button>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- Project List End -->

        <!-- Create Project Form -->
        <!-- Modal -->
        <div class="modal fade" id="createProject" tabindex="-1" aria-labelledby="createProject" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Create Project</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form class="w-100" id="createProjectForm">
                            <div class="mb-3">
                                <label for="name" class="form-label">Project Name</label>
                                <input type="text" class="form-control" id="name" name="name">

                            </div>
                            <div class="mb-3">
                                <label for="inputPassword" class="form-label">Description</label>
                                <textarea type="text" class="form-control" id="description"
                                    name="description"> </textarea>
                            </div>
                            <div class="mb-3">
                                <label for="milestone" class="form-label">Add milestone</label>
                                <textarea type="text" class="form-control" id="milestone" name="milestone"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary w-100 font-bold">Create Project</button>

                        </form>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- Create Project Form End -->

        <!-- Edit Project Form -->
        <!-- Modal -->
        <div class="modal fade" id="editProject" tabindex="-1" aria-labelledby="editProject" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="editProject">Edit Project</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form class="w-100" id="editProjectForm">
                            <div class="mb-3">
                                <label for="name" class="form-label">Project Name</label>
                                <input type="text" class="form-control" id="name" name="name">

                            </div>
                            <div class="mb-3">
                                <label for="inputPassword" class="form-label">Description</label>
                                <textarea type="text" class="form-control" id="description"
                                    name="description"> </textarea>
                            </div>
                            <div class="mb-3">
                                <label for="milestone" class="form-label">Milestone</label>
                                <textarea type="password" class="form-control" id="inputPassword"
                                    name="milestone"></textarea>
                            </div>
                            <div class=" my-4">
                                <label for="status" class="form-label">Status</label>
                                <select class="form-select" aria-label="select role" id="status" name="status">
                                    <option selected>Select status</option>
                                    <option value="awaiting-star">Awaiting Start</option>
                                    <option value="in-progress">In Progress</option>
                                    <option value="on-hold">On Hold</option>
                                </select>
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Edit Project Form End -->

        <!-- Delete Project Form -->
        <!-- Delete Project Form End -->
    </div>

    <script src="../../assets/js/bootstrap.min.js"> </script>
    <script src="../../assets/js/main.js"> </script>


</body>

</html>