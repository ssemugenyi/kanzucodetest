<?php
require_once '../../config.php';

header('Content-Type: application/json');


$method = $_SERVER['REQUEST_METHOD'];

// Endpoint to handle project creation and milestone addition
if ($method === 'POST') {
   
    $data = json_decode(file_get_contents('php://input'), true);
    

// `email` DESC, `password` ASC, `role`
    $stmt = $pdo->prepare('SELECT id, email, role FROM users WHERE email = ? AND password = ?');
    $stmt->execute([$data['email'], $data['password']]);

    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($user);
}