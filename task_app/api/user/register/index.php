<?php
require_once '../../config.php';

header('Content-Type: application/json');


$method = $_SERVER['REQUEST_METHOD'];

// Endpoint to handle user registration
if ($method === 'POST') {
   
    $data = json_decode(file_get_contents('php://input'), true);
    

// `email` DESC, `password` ASC, `role`
    $stmt = $pdo->prepare('INSERT INTO users (email, password, role) VALUES (?, ?, ?)');
    $stmt->execute([$data['email'], $data['password'], $data['role']]);

       echo json_encode(['message' => 'User created successfully']);
}