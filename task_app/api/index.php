<?php

require_once 'config.php';


header('Content-Type: application/json');


$method = $_SERVER['REQUEST_METHOD'];

// Endpoint to handle project creation and milestone addition
if ($method === 'POST') {
   
    $data = json_decode(file_get_contents('php://input'), true);
    

// `project_name`, LEFT(`description`, 256),  `developer_id`,  `project_manager_id`,  `status`,
    $stmt = $pdo->prepare('INSERT INTO projects (project_name, description, developer_id, project_manager_id, status) VALUES (?, ?, ?, ?, ?)');
    $stmt->execute([$data['project_name'], $data['description'], 1, 2, 'awaiting-start']);

       echo json_encode(['message' => 'Project created successfully']);
}

// Endpoint to change project status
elseif ($method === 'PUT') {
    // Get the request body
    $data = json_decode(file_get_contents('php://input'), true);

    $stmt = $pdo->prepare('SELECT status FROM projects WHERE id = ?');
    $stmt->execute([$data['project_id']]);
    $status = $stmt->fetchColumn();

    if ($status !== 'completed') {
     
        $stmt = $pdo->prepare('UPDATE projects SET status = ? WHERE id = ?');
        $stmt->execute([$data['status'], $data['project_id']]);

       
        echo json_encode(['message' => 'Project status updated successfully']);
    } else {
       
        http_response_code(400);
        echo json_encode(['error' => 'Project is already completed and cannot be changed']);
    }
}

// Endpoint to return list of projects assigned to a developer
elseif ($method === 'GET') {
   
    $engineer_id = $_GET['developer_id'];


    $stmt = $pdo->prepare('SELECT * FROM projects WHERE developer_id = ?');
    $stmt->execute([$engineer_id]);
    $projects = $stmt->fetchAll(PDO::FETCH_ASSOC);

   
    echo json_encode($projects);
}

// Endpoint to change engineer assigned to the project
elseif ($method === 'PATCH') {
    // Get the request body
    $data = json_decode(file_get_contents('php://input'), true);
    
 
    $stmt = $pdo->prepare('UPDATE projects SET engineer_id = ? WHERE id = ?');
    $stmt->execute([$data['engineer_id'], $data['project_id']]);

    // Return success response
    echo json_encode(['message' => 'Engineer assigned to project successfully']);
}

// Endpoint to mark a project as completed by the Project Manager
elseif ($method === 'PATCH' && $_SESSION['user_role'] === 'project_manager') {
    // Get the request body
    $data = json_decode(file_get_contents('php://input'), true);
    

    $stmt = $pdo->prepare('UPDATE projects SET status = ? WHERE id = ?');
    $stmt->execute(['completed', $data['project_id']]);

   
    echo json_encode(['message' => 'Project marked as completed successfully']);
}


else {
    http_response_code(405);
    echo json_encode(['error' => 'Method not allowed']);
}
?>