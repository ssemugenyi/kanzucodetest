const loginForm = document.getElementById("loginForm");
const registerForm = document.getElementById("registerForm");
const createProjectForm = document.getElementById("createProjectForm");

/**
 * Login a user
 
 */

loginForm?.addEventListener("submit", async (e) => {
  e.preventDefault();

  const email = document.getElementById("inputEmail").value;
  const password = document.getElementById("inputPassword").value;

  try {
    const res = await fetch(
      "http://kanzucodetest.test/task_app/api/user/login/",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      }
    );

    const user = await res.json();
    localStorage.setItem("userID", user.id);
    localStorage.setItem("userEmail", user.email);
    localStorage.setItem("userRole", user.role);
    window.location.href = "./dashboard";
  } catch (error) {
    console.log(error);
  }
});

/**
 * Register a user
 */

registerForm?.addEventListener("submit", async (e) => {
  e.preventDefault();

  const email = document.getElementById("inputEmail").value;
  const password = document.getElementById("inputPassword").value;
  const role = document.getElementById("role").value;

  try {
    const res = await fetch(
      "http://kanzucodetest.test/task_app/api/user/register/",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
          role: role,
        }),
      }
    );

    console.log(res);
    window.location.href = "./";
  } catch (error) {
    console.log(error);
  }
});

/**
 * Create a project
 */
createProjectForm?.addEventListener("submit", async (e) => {
  e.preventDefault();

  const project_name = document.getElementById("name").value;
  const description = document.getElementById("description").value;
  const milestone = document.getElementById("milestone").value;
  const dev_id = localStorage.getItem("userID");

  console.log();

  console.log(project_name, description, milestone, dev_id);

  try {
    const res = await fetch("http://kanzucodetest.test/task_app/api/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      //   `project_name`, LEFT(`description`, 256),  `developer_id`,  `project_manager_id`,  `status`,
      body: JSON.stringify({
        project_name: project_name,
        description: description,
        developer_id: dev_id,
        project_manager_id: 2,
      }),
    });
    console.log(res);
  } catch (error) {
    console.log(error);
  }
});

const getProjects = async () => {
  try {
    const res = await fetch(
      "http://kanzucodetest.test/task_app/api/?developer_id=1",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    console.log(res);
  } catch (error) {
    console.log(error);
  }
};

getProjects();

console.log("Hello from main.js");
