<?php

/**
 * @name removeDuplicatesFromFile
 * @description Function to read text from file and return an array without duplicate items
 * @param $filename
 * @return unique_words
 */
function removeDuplicatesFromFile($filename) {
 ($filename);
    $text = file_get_contents($filename);
 
    $words = preg_split('/\s+/', $text);
  
    $unique_words = array_unique($words);
    return $unique_words;
}


/**
 * @name getPunctuationMarksFromFile
 * @description Function to read text from file and return an array with only punctuation marks
 * @param $filename
 * @return punctuation_marks
 */
function getPunctuationMarksFromFile($filename) {
  
    $text = file_get_contents($filename);
  
    preg_match_all("/[[:punct:]]/", $text, $matches);

    $punctuation_marks = array_unique($matches[0]);
    return $punctuation_marks;
}

// Test the functions

$filename = 'test-file.txt';
$text = "This is a test file with some dummy text. This text file is small! It's also very brief, do you want to add some more text to it?";
file_put_contents($filename, $text);

function elementsDisplay($array) {
    foreach ($array as $element) {
        echo "<p>$element</p> ";
    }
}




$unique_words = removeDuplicatesFromFile($filename);
echo "Unique words from the file:\n";

elementsDisplay($unique_words);
// print_r($unique_words);


$punctuation_marks = getPunctuationMarksFromFile($filename);
echo "\nPunctuation marks from the file:\n";
// print_r($punctuation_marks);

elementsDisplay($punctuation_marks);

?>