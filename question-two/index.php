<?php
$paragraph = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";

function getEmailWithRegex($paragraph) {
    $words = explode(" ", $paragraph);
    foreach ($words as $word) {
        if (strpos($word, '@') !== false && strpos($word, '.') !== false) {
            return $word;
        }
    }
    return null;
}

echo getEmailWithRegex($paragraph);
echo "\n";  

function getPhoneWithRegex($paragraph) {
    preg_match('/\b\d{12}\b/', $paragraph, $matches);
    return $matches[0] ?? null;
}
echo getPhoneWithRegex($paragraph) . "\n";  

echo "\n";  


function getUrlWithRegex($paragraph) {
    $words = explode(" ", $paragraph);
    foreach ($words as $word) {
        if (strpos($word, "http://") === 0 || strpos($word, "https://") === 0) {
            return $word;
        }
    }
    return null;
}

echo getUrlWithRegex($paragraph);
echo "\n";  

echo "<br>";



/**
 * @name getEmailWithoutRegex
 * @description Function to extract email from a paragraph without using regex
 * @param $paragraph
 * @return email
 */
function getEmailWithoutRegex($paragraph) {
    $words = explode(" ", $paragraph);
    foreach ($words as $word) {
        if (filter_var($word, FILTER_VALIDATE_EMAIL)) {
            return $word;
        }
    }
    return "";
}

/**
 * @name getPhoneNumberWithoutRegex
 * @description Function to extract phone number from a paragraph without using regex
 * @param $paragraph
 * @return phone_number
 
 */

function getPhoneNumberWithoutRegex($paragraph) {
    $words = explode(" ", $paragraph);
    foreach ($words as $word) {
        if (preg_match('/\d{10,}/', $word)) {
            return $word;
        }
    }
    return "";
}

/**
 * @name getUrlWithoutRegex
 * @description Function to extract URL from a paragraph without using regex
 * @param $paragraph
 * @return url
 */
function getUrlWithoutRegex($paragraph) {
    $words = explode(" ", $paragraph);
    foreach ($words as $word) {
        if (filter_var($word, FILTER_VALIDATE_URL)) {
            return $word;
        }
    }
    return "";
}



echo "Email: " . getEmailWithoutRegex($paragraph) . "\n";
echo "Phone Number: " . getPhoneNumberWithoutRegex($paragraph) . "\n";
echo "URL: " . getUrlWithoutRegex($paragraph) . "\n";