<?php
/**
 * @name fibonacci
 * @description Function to generate fibonacci numbers
 * @param $n
 */

function fibonacci($n) {
    $fib = [0, 1];
    for ($i = 2; $i < $n; $i++) {
        $fib[$i] = $fib[$i - 1] + $fib[$i - 2];
    }
    return $fib;
}


$array = range(0, 100);
rsort($array);


$fibonacci_numbers = array_slice(fibonacci(20), 6);

print_r($fibonacci_numbers);