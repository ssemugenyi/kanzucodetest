<?php
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
</head>

<body>

    <div class="d-flex align-items-center gap-4 justify-content-center vh-100">

        <a href="./question-one" class="btn btn-primary">Question one</a>
        <a href="./question-two" class="btn btn-primary">Question two</a>
        <a href="./read.php" class="btn btn-primary">Question three</a>
        <a href="./task_app" class="btn btn-primary">Question Four</a>
        <a href="./jaba" class="btn btn-primary">Question Five</a>
    </div>


    <script src="../assets/js/bootstrap.min.js">
    </script>

</body>

</html>